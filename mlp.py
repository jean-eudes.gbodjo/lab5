import sys
import time
import os
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def format_X(lst_X, n_timestamps=37):
    X = None
    for i in range(len(lst_X)):
        tmp = lst_X[i]
        if X is None :
            X = tmp
        else :
            X = np.vstack((X,tmp))
    X = X.reshape(X.shape[0],n_timestamps,-1)
    print (new_X.shape)
    return new_X


def format_y (lst_y,target,scale_by=1):
    y = None
    for i in range(len(lst_y)):
        tmp = lst_y[i]
        if y is None :
            y = tmp[target]
        else :
            y = np.hstack((y,tmp[target]))
    print (y.shape)
    return y/scale_by




if __name__ == '__main__' :

    # Reading data
    train_radar_X1 = np.load(sys.argv[1])
    train_opt_X1 = np.load(sys.argv[2])
    train_indices_X1 = np.load(sys.argv[3])
    train_y1 = np.load(sys.argv[4])

    train_radar_X2 = np.load(sys.argv[5])
    train_opt_X2 = np.load(sys.argv[6])
    train_indices_X2 = np.load(sys.argv[7])
    train_y2 = np.load(sys.argv[8])

    sys.stdout.flush

    # Formatting
    train_radar_X = 
    test

    # Run Model
    units = 128
    batch_size = 8
    n_epochs = 500
    learning_rate = 1E-4
    drop = 0.3

    # run()