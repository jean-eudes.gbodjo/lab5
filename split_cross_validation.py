import random
from datetime import datetime
import numpy as np
import os

def writeSplit (idx, data, outFileName):
    fold = data[idx]
    np.save(outFileName, fold)
    
def split_data (radar_seq,opt_seq,indices_seq,yields,n_folds=3,n_random=10) :
    '''
    '''
    ptrn = os.path.basename(radar_seq).split("_")[0]+"_"+os.path.basename(radar_seq).split("_")[1]
    if not os.path.exists (os.path.join("splits",ptrn)):
        os.makedirs(os.path.join("splits",ptrn))
    print (ptrn)

    y = np.load(yields)
    radar = np.load(radar_seq)
    opt = np.load(opt_seq)
    indices = np.load(indices_seq)

    idx = np.arange(y.shape[0])
    idx_random = np.arange(y.shape[0])

    n_samples = int(y.shape[0]/n_folds)

    for j in range(n_random) :
        dt = datetime.now()
        random.seed(dt.microsecond)
        for i in range(n_folds):
            if i==0:
                random.shuffle(idx_random)
                random.shuffle(idx_random)
            
            test_samples = idx_random[i*n_samples:(i+1)*n_samples]
            
            test_idx = np.where(np.isin(idx,test_samples))
            train_idx = np.where(np.isin(idx,test_samples,invert=True))

            writeSplit(train_idx, radar, os.path.join("splits",ptrn,"train_radar_{}_fold-{}.npy".format(j+1,i+1)))
            writeSplit(test_idx, radar, os.path.join("splits",ptrn,"test_radar_{}_fold-{}.npy".format(j+1,i+1)))

            writeSplit(train_idx, opt, os.path.join("splits",ptrn,"train_opt_{}_fold-{}.npy".format(j+1,i+1)))
            writeSplit(test_idx, opt, os.path.join("splits",ptrn,"test_opt_{}_fold-{}.npy".format(j+1,i+1)))

            writeSplit(train_idx, indices, os.path.join("splits",ptrn,"train_indices_{}_fold-{}.npy".format(j+1,i+1)))
            writeSplit(test_idx, indices, os.path.join("splits",ptrn,"test_indices_{}_fold-{}.npy".format(j+1,i+1)))

            writeSplit(train_idx, y, os.path.join("splits",ptrn,"train_yields_{}_fold-{}.npy".format(j+1,i+1)))
            writeSplit(test_idx, y, os.path.join("splits",ptrn,"test_yields_{}_fold-{}.npy".format(j+1,i+1)))

        print ("========= split {} =========".format(j+1))
        print ("%d-folds : %d, %d over %d" % (n_folds,train_idx[0].shape[0], test_idx[0].shape[0], len(idx)))

if __name__ == "__main__":

    # Niakhar 2017
    radar_seq = "./data/niakhar_2017_rad.npy"
    opt_seq = "./data/niakhar_2017_opt.npy"
    indices_seq = "./data/niakhar_2017_indices.npy"
    yields = "./data/niakhar_2017_yields.npy"

    split_data (radar_seq,opt_seq,indices_seq,yields)

    # # Niakhar 2018
    # radar_seq = "./data/niakhar_2018_rad.npy"
    # opt_seq = "./data/niakhar_2018_opt.npy"
    # indices_seq = "./data/niakhar_2018_indices.npy"
    # yields = "./data/niakhar_2018_yields.npy"

    # split_data (radar_seq,opt_seq,indices_seq,yields)

    # Niakhar 2018 # SIMCO
    radar_seq = "./data/niakhar-simco_2018_rad.npy"
    opt_seq = "./data/niakhar-simco_2018_opt.npy"
    indices_seq = "./data/niakhar-simco_2018_indices.npy"
    yields = "./data/niakhar-simco_2018_yields.npy"

    split_data (radar_seq,opt_seq,indices_seq,yields)

    # Niakhar 2018 # SERENA
    radar_seq = "./data/niakhar-serena_2018_rad.npy"
    opt_seq = "./data/niakhar-serena_2018_opt.npy"
    indices_seq = "./data/niakhar-serena_2018_indices.npy"
    yields = "./data/niakhar-serena_2018_yields.npy"

    split_data (radar_seq,opt_seq,indices_seq,yields)

    # # Nioro 2018
    # radar_seq = "./data/nioro_2018_rad.npy"
    # opt_seq = "./data/nioro_2018_opt.npy"
    # indices_seq = "./data/nioro_2018_indices.npy"
    # yields = "./data/nioro_2018_yields.npy"

    # split_data (radar_seq,opt_seq,indices_seq,yields)