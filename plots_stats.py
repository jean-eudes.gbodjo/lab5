import gdal, ogr, osr
import rasterio
import numpy as np
import os
import sys
import pandas as pd
import geopandas
import datetime

def ZonalStats (plots_file,lstTS,dates_file,outPath,outPattern,tree_mask=None):
    '''
    '''
    print(outPattern)
    df = geopandas.read_file(plots_file)

    lstDates = []
    f = open(dates_file)
    for d in f :
        lstDates.append(d[:8])

    rmem_drv = gdal.GetDriverByName("MEM")
    vmem_drv = ogr.GetDriverByName("MEMORY")

    outdf = None
    ggplot_dic = {}
    for j in range(len(lstTS)) :
        with rasterio.open(lstTS[j]) as ds:
            band = os.path.basename(lstTS[j]).split('_',1)[0]
            count = ds.count
            nodata = ds.profile['nodata']
            cellsize = ds.profile['transform'][0]
            originX = ds.profile['transform'][2]
            originY = ds.profile['transform'][5]

            crs = df.crs
            epsg = int(crs['init'].split(":")[-1]) 
            Proj = osr.SpatialReference()
            Proj.ImportFromEPSG(epsg)

            dic = {}
            for index, row in df.iterrows():
                extent = row.geometry.bounds
                ulx = extent[0]
                uly = extent[3]
                lrx = extent[2]
                lry = extent[1]
                
                vGeoT = (ulx,cellsize,0,uly,0,-cellsize)    
                xOffset = int((ulx - originX) / cellsize)
                yOffset = int((uly - originY) / -cellsize)
                
                col_nb = int((lrx - ulx)/cellsize) # Raster xsize col
                row_nb = int((uly - lry)/cellsize)  # Raster ysize lines

                window = rasterio.windows.Window(xOffset,yOffset,col_nb,row_nb)
                
                if col_nb >= 1 and row_nb >= 1 and xOffset < ds.width and xOffset+col_nb < ds.width and yOffset < ds.height and yOffset+row_nb < ds.height :
                    # Create Raster in Memory for feature Rasterization
                    rfeat_ds = rmem_drv.Create('', col_nb, row_nb, 1, gdal.GDT_Byte)
                    rfeat_ds.SetGeoTransform(vGeoT)
                    rfeat_ds.SetProjection(Proj.ExportToWkt())

                    # Create a temporary vector layer in memory containing current feature
                    vfeat_ds = vmem_drv.CreateDataSource('Out')
                    vfeat_layer = vfeat_ds.CreateLayer('Polygon', Proj, ogr.wkbPolygon)
                    vfeat_layerDefn = vfeat_layer.GetLayerDefn()
                    feature = ogr.Feature(vfeat_layerDefn)
                    feature.SetGeometry(ogr.CreateGeometryFromWkt(str(row.geometry)))
                    vfeat_layer.CreateFeature(feature)

                    # Rasterize feature
                    gdal.RasterizeLayer(rfeat_ds, [1], vfeat_layer, burn_values=[1])
                    rfeatArray = rfeat_ds.ReadAsArray()
                    if np.count_nonzero(rfeatArray) > 1 :
                        dic.setdefault('feat_index',[]).append(index)
                        if j == 0:
                            dic.setdefault('ID',[]).append(row['ID'])
                            dic.setdefault('Projet',[]).append(row['Projet'])
                            dic.setdefault('Biom_f',[]).append(row['Biomasse_f'])
                            dic.setdefault('Biom_s',[]).append(row['Biomasse_s'])
                            dic.setdefault('Rdt_f',[]).append(row['Rdt_f'])
                            dic.setdefault('Rdt_s',[]).append(row['Rdt_s'])
                            dic.setdefault('Sowing',[]).append(row['Semi'])
                            dic.setdefault('Harvest',[]).append(row['Recolte'])

                        ts_values = ds.read(tuple(np.arange(count)+1),window=window)
                        # Tree_Mask 
                        if tree_mask is not None:
                            with rasterio.open(tree_mask) as tm :
                                tm_cellsize = tm.profile['transform'][0]
                                tm_originX = tm.profile['transform'][2]
                                tm_originY = tm.profile['transform'][5]
                                tm_xOffset = int((ulx - tm_originX) / tm_cellsize)
                                tm_yOffset = int((uly - tm_originY) / -tm_cellsize)
                                tm_col_nb = int((lrx - ulx)/tm_cellsize)
                                tm_row_nb = int((uly - lry)/tm_cellsize)
                                tm_window = rasterio.windows.Window(tm_xOffset,tm_yOffset,tm_col_nb,tm_row_nb)
                                tree_mask_array = tm.read(1,window=tm_window)
                            masked = np.ma.MaskedArray(ts_values,mask=np.logical_or(ts_values == nodata,np.logical_not(rfeatArray)*tree_mask_array))
                        else :
                            masked = np.ma.MaskedArray(ts_values,mask=np.logical_or(ts_values == nodata,np.logical_not(rfeatArray)))
                        masked = masked.reshape(count,-1)
                        mean = np.mean(masked,axis=1)
                        std = np.std(masked,axis=1)

                        for d in range(len(lstDates)):
                            dic.setdefault('%s_%s_Mean'%(band,lstDates[d]),[]).append(mean[d])
                            dic.setdefault('%s_%s_Std'%(band,lstDates[d]),[]).append(std[d])

                            ggplot_dic.setdefault('Date',[]).append(datetime.datetime.strptime(lstDates[d],"%Y%m%d").strftime("%Y-%m-%d"))
                            ggplot_dic.setdefault('Band',[]).append(band)
                            ggplot_dic.setdefault('Plot',[]).append(row['ID'])
                            ggplot_dic.setdefault('Projet',[]).append(row['Projet'])
                            # ggplot_dic.setdefault('Crop',[]).append(row['Crop'])
                            ggplot_dic.setdefault('Sowing',[]).append(row['Semi'])
                            ggplot_dic.setdefault('Harvest',[]).append(row['Recolte'])
                            ggplot_dic.setdefault('Mean',[]).append(mean[d])
                            ggplot_dic.setdefault('Std',[]).append(std[d])
        rfeat_ds = None
        vfeat_ds = None

        if outdf is None :
            outdf = pd.DataFrame.from_dict(dic)
        else:
            outdf = outdf.merge(pd.DataFrame.from_dict(dic),on='feat_index',how='inner')

    rmem_drv = None
    vmem_drv = None 
 
    
    outPath = os.path.join(outPath,"stats")
    if not os.path.exists(outPath):
        os.makedirs(outPath)
    dataCSV = os.path.join(outPath,'{}.csv'.format(outPattern))
    outdf = outdf.drop(columns='feat_index')
    outdf.to_csv(dataCSV,index=False)

    # To ggplot
    plotCSV = os.path.join(outPath,'{}_toggplot.csv'.format(outPattern))

    ggplot_df = pd.DataFrame.from_dict(ggplot_dic)
    ggplot_df = ggplot_df.loc[ggplot_df.Plot.isin(outdf['ID'])]
    ggplot_df.to_csv(plotCSV,index=False)

if __name__=="__main__":

    outPath = "/media/je/SATA_1/lab5"
    
    # -----
    # 2017
    # -----

        # --------
        # Niakhar
        # --------
    
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2017.shp"
    tree_mask = "/media/je/LACIE/SENEGAL/RENDEMENTS/Tree_Mask_Pleiade_Niakhar_2018_05_10m.tif"
    
            # ------
            # Radar
            # ------

    outPattern = "niakhar_2017_radar"
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2017/OUTPUT/CONCAT/VH_ASC_CONCAT_S1.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2017/OUTPUT/CONCAT/VV_ASC_CONCAT_S1.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2017/OUTPUT/CONCAT/dates_ASC.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)
    
    outPattern = "niakhar_2017_radar_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

            # --------
            # Optical 
            # --------

    outPattern = "niakhar_2017_opt"
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B2_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B3_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B4_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B8_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B5_NIAKHAR_2017_CONCAT_S2-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B6_NIAKHAR_2017_CONCAT_S2-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B7_NIAKHAR_2017_CONCAT_S2-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B8A_NIAKHAR_2017_CONCAT_S2-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B11_NIAKHAR_2017_CONCAT_S2-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B12_NIAKHAR_2017_CONCAT_S2-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/NDVI_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/NDWI_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/EVI_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/MSAVI2_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/GDVI_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/CIGreen_NIAKHAR_2017_CONCAT_S2.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/CIRedEdge_NIAKHAR_2017_CONCAT_S2.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/dates.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "niakhar_2017_opt_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

            # ------------------------
            # Optical with Gapfilling
            # ------------------------

    outPattern = "niakhar_2017_opt_gapf"
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B2_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B3_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B4_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B8_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B5_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B6_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B7_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B8A_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B11_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B12_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/NDVI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/NDWI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/EVI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/MSAVI2_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/GDVI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/CIGreen_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/CIRedEdge_NIAKHAR_2017_CONCAT_S2_GAPF.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/dates.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "niakhar_2017_opt_gapf_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    # -----
    # 2018
    # -----

        # --------
        # Niakhar
        # --------
    
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2018.shp"
    tree_mask = "/media/je/LACIE/SENEGAL/RENDEMENTS/Tree_Mask_Pleiade_Niakhar_2018_05_10m.tif"
    
            # ------
            # Radar
            # ------

    outPattern = "niakhar_2018_radar"
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VH_ASC_CONCAT_S1.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VV_ASC_CONCAT_S1.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/dates_ASC.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)
    
    outPattern = "niakhar_2018_radar_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    ############
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_radar"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_radar_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_radar"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_radar_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)
    #############

            # --------
            # Optical
            # --------
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2018.shp"
    outPattern = "niakhar_2018_opt"
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B2_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B3_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B4_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B5_NIAKHAR_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B6_NIAKHAR_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B7_NIAKHAR_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8A_NIAKHAR_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B11_NIAKHAR_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B12_NIAKHAR_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/NDVI_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/NDWI_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/EVI_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/MSAVI2_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/GDVI_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/CIGreen_NIAKHAR_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/CIRedEdge_NIAKHAR_2018_CONCAT_S2.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/dates.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "niakhar_2018_opt_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    ############
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_opt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_opt_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_opt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_opt_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)
    #############

            # ------------------------
            # Optical with gapfilling
            # ------------------------
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2018.shp"
    outPattern = "niakhar_2018_opt_gapf"
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B2_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B3_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B4_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B5_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B6_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B7_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8A_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B11_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B12_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/NDVI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/NDWI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/EVI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/MSAVI2_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/GDVI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/CIGreen_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/CIRedEdge_NIAKHAR_2018_CONCAT_S2_GAPF.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/dates.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "niakhar_2018_opt_gapf_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    ############
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_opt_gapf"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_opt_gapf_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_opt_gapf"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_opt_gapf_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)
    #############

    # --------
    # Nioro
    # --------

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Nioro_2018.shp"
    tree_mask = "/media/je/LACIE/SENEGAL/RENDEMENTS/Tree_Mask_Planet_Nioro_2018_06_03_10m.tif"

            # ------
            # Radar
            # ------

    outPattern = "nioro_2018_radar"
    lstTS = ["/media/je/LACIE/SENEGAL/NIORO/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VH_ASC_CONCAT_S1.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VV_ASC_CONCAT_S1.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/dates_ASC.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "nioro_2018_radar_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

            # --------
            # Optical
            # --------

    outPattern = "nioro_2018_opt"
    lstTS = ["/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B2_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B3_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B4_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B5_NIORO_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B6_NIORO_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B7_NIORO_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8A_NIORO_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B11_NIORO_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B12_NIORO_2018_CONCAT_S2-10m.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/NDVI_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/NDWI_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/EVI_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/MSAVI2_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/GDVI_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/CIGreen_NIORO_2018_CONCAT_S2.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/CIRedEdge_NIORO_2018_CONCAT_S2.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/dates.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "nioro_2018_opt_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)

            # ------------------------
            # Optical with gapfilling
            # ------------------------

    outPattern = "nioro_2018_opt_gapf"
    lstTS = ["/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B2_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B3_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B4_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B5_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B6_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B7_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8A_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B11_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B12_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/NDVI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/NDWI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/EVI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/MSAVI2_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/GDVI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/CIGreen_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/CIRedEdge_NIORO_2018_CONCAT_S2_GAPF.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/dates.txt"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern)

    outPattern = "nioro_2018_opt_gapf_notree"
    ZonalStats(plots_file,lstTS,dates_file,outPath,outPattern,tree_mask)
