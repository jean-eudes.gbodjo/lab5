import os
import numpy as np
import pandas as pd
import scipy as sp
from scipy import sparse, linalg
import seaborn as sns
import matplotlib.pyplot as plt
sns.set()

def interpolate_ts (csv,lstBands):
    
    ptrn = os.path.basename(csv)
    year = ptrn.split('_')[1]
    outFolder = "./interpolate"
    if not os.path.exists(outFolder):
        os.makedirs(outFolder)

    df = pd.read_csv(csv)

    outdf = None
    ggplot_dic = {}
    for band in lstBands:
        cols = [col for col in df.columns if col.startswith(band) and col.endswith("Mean")]
        cols.sort()
        
        times = pd.date_range(start="{}0502".format(year), end = "{}1030".format(year), freq='D')
        dic = {}

        for index, row in df.iterrows():
            lstValues = []
            for j in range(len(times)): # dates
                date = str(times.strftime('{}_%Y%m%d_Mean'.format(band))[j])
                if date in cols: 
                    lstValues.append(row[date])
                else :
                    lstValues.append(np.nan)
            upsampled = pd.Series(lstValues,index=times)
            interpolated = upsampled.interpolate(method='linear',limit_direction='both')
            resampled = interpolated.resample('5D').mean()
            
            dic.setdefault('feat_index',[]).append(index)
            if band == lstBands[0] :
                dic.setdefault('ID',[]).append(row['ID'])
                dic.setdefault('Projet',[]).append(row['Projet'])
                dic.setdefault('Biom_f',[]).append(row['Biom_f'])
                dic.setdefault('Biom_s',[]).append(row['Biom_s'])
                dic.setdefault('Rdt_f',[]).append(row['Rdt_f'])
                dic.setdefault('Rdt_s',[]).append(row['Rdt_s'])
            for idx , value in resampled.items():
                dic.setdefault('{}_{}'.format(band,idx.strftime("%Y%m%d")),[]).append(value)

                ggplot_dic.setdefault('Date',[]).append(idx.strftime("%Y-%m-%d"))
                ggplot_dic.setdefault('Band',[]).append(band)
                ggplot_dic.setdefault('Plot',[]).append(row['ID'])
                ggplot_dic.setdefault('Projet',[]).append(row['Projet'])
                # ggplot_dic.setdefault('Crop',[]).append(row['Crop'])
                ggplot_dic.setdefault('Sowing',[]).append(row['Sowing'])
                ggplot_dic.setdefault('Harvest',[]).append(row['Harvest'])
                ggplot_dic.setdefault('Value',[]).append(value)
        
        if outdf is None :
            outdf = pd.DataFrame.from_dict(dic)
        else:
            outdf = outdf.merge(pd.DataFrame.from_dict(dic),on='feat_index',how='inner')

    outCSV = os.path.join(outFolder,'{}'.format(ptrn.replace(".csv","_interpolate.csv")))
    outdf = outdf.drop(columns='feat_index')
    outdf.to_csv(outCSV,index=False)

    # To ggplot
    plotCSV = os.path.join(outFolder,'{}'.format(ptrn.replace(".csv","_interpolate_toggplot.csv")))
    ggplot_df = pd.DataFrame.from_dict(ggplot_dic)
    ggplot_df = ggplot_df.loc[ggplot_df.Plot.isin(outdf['ID'])]
    ggplot_df.to_csv(plotCSV,index=False)

if __name__=='__main__':

    csv = "./stats/niakhar_2017_opt_gapf_notree.csv"
    interpolate_ts(csv,["B2","B3","B4","B8","B5","B6","B7","B8A","B11","B12","NDVI","NDWI","EVI","MSAVI2","GDVI","CIGreen","CIRedEdge"])

    csv = "./stats/niakhar_2017_radar_notree.csv"
    interpolate_ts(csv,["VH","VV"])

    csv = "./stats/niakhar_2018_opt_gapf_notree.csv"
    interpolate_ts(csv,["B2","B3","B4","B8","B5","B6","B7","B8A","B11","B12","NDVI","NDWI","EVI","MSAVI2","GDVI","CIGreen","CIRedEdge"])

    csv = "./stats/niakhar_2018_radar_notree.csv"
    interpolate_ts(csv,["VH","VV"])

    csv = "./stats/niakhar-serena_2018_opt_gapf_notree.csv"
    interpolate_ts(csv,["B2","B3","B4","B8","B5","B6","B7","B8A","B11","B12","NDVI","NDWI","EVI","MSAVI2","GDVI","CIGreen","CIRedEdge"])

    csv = "./stats/niakhar-serena_2018_radar_notree.csv"
    interpolate_ts(csv,["VH","VV"])

    csv = "./stats/niakhar-simco_2018_opt_gapf_notree.csv"
    interpolate_ts(csv,["B2","B3","B4","B8","B5","B6","B7","B8A","B11","B12","NDVI","NDWI","EVI","MSAVI2","GDVI","CIGreen","CIRedEdge"])

    csv = "./stats/niakhar-simco_2018_radar_notree.csv"
    interpolate_ts(csv,["VH","VV"])

    csv = "./stats/nioro_2018_opt_gapf_notree.csv"
    interpolate_ts(csv,["B2","B3","B4","B8","B5","B6","B7","B8A","B11","B12","NDVI","NDWI","EVI","MSAVI2","GDVI","CIGreen","CIRedEdge"])

    csv = "./stats/nioro_2018_radar_notree.csv"
    interpolate_ts(csv,["VH","VV"])