import numpy as np 
import matplotlib.pyplot as plt 

opt = "./patchs_data/1Ha/niakhar_2018_opt_patchs.npy"
array = np.load(opt)
print (array.shape)

# for i in range(array.shape[-1]):
subset = array[47,:100,3,:]
# print (subset.shape)
subset = subset.reshape(10,10,18)
# print (subset.shape)

plt.figure()
plt.imshow(subset[:,:,0],cmap='gray')
plt.grid(False)
plt.axis('off')
plt.show()

plt.figure()
plt.imshow(subset[:,:,9],cmap='gray')
plt.grid(False)
plt.axis('off')
plt.show()

plt.figure()
plt.imshow(subset[:,:,17],cmap='gray')
plt.grid(False)
plt.axis('off')
plt.show()