import gdal, ogr, osr
import rasterio
import numpy as np
import os
import sys
import pandas as pd
import geopandas
import datetime

def ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area,plots_info=False):
    '''
    '''
    print(outPattern,plots_info)
    df = geopandas.read_file(plots_file)

    lstDates = []
    f = open(dates_file)
    for d in f :
        lstDates.append(d[:8])

    size = int(np.sqrt(area * 10000))

    ts_array = None
    dic = {}
    for j in range(len(lstTS)) :
        with rasterio.open(lstTS[j]) as ds:
            # band = os.path.basename(lstTS[j]).split('_',1)[0]
            count = ds.count
                
            cellsize = ds.profile['transform'][0]
            originX = ds.profile['transform'][2]
            originY = ds.profile['transform'][5]

            crs = df.crs
            epsg = int(crs['init'].split(":")[-1]) 
            Proj = osr.SpatialReference()
            Proj.ImportFromEPSG(epsg)

            tmp_array = None

            for index, row in df.iterrows():
                centroid = row.geometry.centroid

                ulx = centroid.x - size/2
                uly = centroid.y + size/2
            
                xOffset = int((ulx - originX) / cellsize)
                yOffset = int((uly - originY) / -cellsize)

                col_nb=row_nb=int(size/cellsize)
                window = rasterio.windows.Window(xOffset,yOffset,col_nb,row_nb)
            
                if xOffset < ds.width and xOffset+col_nb < ds.width and yOffset < ds.height and yOffset+row_nb < ds.height :
                    
                    if j == 0 and plots_info :
                        dic.setdefault('feat_index',[]).append(index)
                        dic.setdefault('ID',[]).append(row['ID'])
                        dic.setdefault('Projet',[]).append(row['Projet'])
                        dic.setdefault('Biom_f',[]).append(row['Biomasse_f'])
                        dic.setdefault('Biom_s',[]).append(row['Biomasse_s'])
                        dic.setdefault('Rdt_f',[]).append(row['Rdt_f'])
                        dic.setdefault('Rdt_s',[]).append(row['Rdt_s'])
                        dic.setdefault('Sowing',[]).append(row['Semi'])
                        dic.setdefault('Harvest',[]).append(row['Recolte'])
                        
                    ts_values = None
                    for c in range(count) :
                        if ts_values is None :
                            ts_values = ds.read(c+1,window=window).reshape(col_nb*row_nb)
                        else :
                            ts_values = np.hstack((ts_values,ds.read(c+1,window=window).reshape(col_nb*row_nb)))

                    if tmp_array is None:
                        tmp_array = ts_values
                    else:
                        tmp_array = np.vstack((tmp_array,ts_values))
        
        if ts_array is None:
            ts_array = tmp_array
        else :
            ts_array = np.hstack((ts_array,tmp_array))

    outPath = "./patchs/{}Ha".format(area)
    if not os.path.exists(outPath):
        os.makedirs(outPath)

    if plots_info :
        ptrn = outPattern.split('_')[0]+'_'+outPattern.split('_')[1]
        outCSV = os.path.join(outPath,'{}_plots.csv'.format(ptrn))
        outdf = pd.DataFrame.from_dict(dic)
        outdf = outdf.drop(columns='feat_index')
        outdf.to_csv(outCSV,index=False)

    np.save(os.path.join(outPath,'{}.npy'.format(outPattern)),ts_array)

if __name__=="__main__":

    # 2017
    # Niakhar
    # Radar
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2017.shp"
    outPath = "/media/je/SATA_1/lab5"
    area=1

    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2017/OUTPUT/CONCAT/VH_ASC_CONCAT_S1.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2017/OUTPUT/CONCAT/VV_ASC_CONCAT_S1.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2017/OUTPUT/CONCAT/dates_ASC.txt"

    outPattern = "niakhar_2017_radar"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area,plots_info=True)
    
    # Optical
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B2_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B3_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B4_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B8_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B5_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B6_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B7_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B8A_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B11_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/B12_NIAKHAR_2017_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/NDVI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/NDWI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/EVI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/MSAVI2_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/GDVI_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/CIGreen_NIAKHAR_2017_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/INDICES/CIRedEdge_NIAKHAR_2017_CONCAT_S2_GAPF.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2017/OUTPUT/CONCAT_GAPF/dates.txt"
    
    outPattern = "niakhar_2017_opt_gapf"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area)
    
    # 2018 
    # Niakhar
    # Radar

    outPath = "/media/je/SATA_1/lab5"
    area=1

    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VH_ASC_CONCAT_S1.tif",
             "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VV_ASC_CONCAT_S1.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/dates_ASC.txt"

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2018.shp"
    outPattern = "niakhar_2018_radar"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area,plots_info=True)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_radar"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area,plots_info=True)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_radar"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area,plots_info=True)

    # Optical
    lstTS = ["/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B2_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B3_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B4_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B5_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B6_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B7_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8A_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B11_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B12_NIAKHAR_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/NDVI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/NDWI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/EVI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/MSAVI2_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/GDVI_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/CIGreen_NIAKHAR_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/INDICES/CIRedEdge_NIAKHAR_2018_CONCAT_S2_GAPF.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIAKHAR/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/dates.txt"
    
    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar_2018.shp"
    outPattern = "niakhar_2018_opt_gapf"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SERENA_2018.shp"
    outPattern = "niakhar-serena_2018_opt_gapf"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area)

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Niakhar-SIMCO_2018.shp"
    outPattern = "niakhar-simco_2018_opt_gapf"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area)

    # 2018 
    # Nioro
    # Radar

    plots_file = "/media/je/LACIE/SENEGAL/RENDEMENTS/Rdt_Mil_Nioro_2018.shp"
    outPath = "/media/je/SATA_1/lab5"
    area=1

    lstTS = ["/media/je/LACIE/SENEGAL/NIORO/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VH_ASC_CONCAT_S1.tif",
             "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/VV_ASC_CONCAT_S1.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-1/ASC/2018/OUTPUT/CONCAT/dates_ASC.txt"
    
    outPattern = "nioro_2018_radar"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area,plots_info=True)

    # Optical
    lstTS = ["/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B2_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B3_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B4_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B5_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B6_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B7_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B8A_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B11_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/B12_NIORO_2018_CONCAT_S2_GAPF-10m.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/NDVI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/NDWI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/EVI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/MSAVI2_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/GDVI_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/CIGreen_NIORO_2018_CONCAT_S2_GAPF.tif",
                "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/INDICES/CIRedEdge_NIORO_2018_CONCAT_S2_GAPF.tif"]
    dates_file = "/media/je/LACIE/SENEGAL/NIORO/SENTINEL-2/2018/OUTPUT/CONCAT_GAPF/dates.txt"
    
    outPattern = "nioro_2018_opt_gapf"
    ExtractPatch(plots_file,lstTS,dates_file,outPath,outPattern,area)