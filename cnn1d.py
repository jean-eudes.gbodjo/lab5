import sys
import time
import os
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def format_X_3D(lst_X, n_timestamps=37):
    X = None
    for i in range(len(lst_X)):
        tmp = lst_X[i]
        if X is None :
            X = tmp
        else :
            X = np.vstack((X,tmp))
    X = X.reshape(X.shape[0],n_timestamps,-1)
    print (X.shape)
    return X

def format_y (lst_y,target,scale_by=1000):
    y = None
    for i in range(len(lst_y)):
        tmp = lst_y[i]
        if y is None :
            y = tmp[:,target]
        else :
            y = np.hstack((y,tmp[:,target]))
    print (y.shape)
    return y/scale_by

def conv1d (X, n_filters, k_size, name, padding_mode="valid", strides=False, activate=True, bias=True) :
    if strides :
        conv = tf.keras.layers.Conv1D(filters=n_filters, kernel_size=k_size, strides= 2, padding=padding_mode, use_bias=bias, name=name)(X)
    else :
        conv = tf.keras.layers.Conv1D(filters=n_filters, kernel_size=k_size, padding=padding_mode, use_bias=bias, name=name)(X)
    # conv = tf.keras.layers.BatchNormalization(name="%s_batchnorm"%name)(conv)
    if activate :
        conv = tf.nn.relu(conv,name="%s_relu"%name)
    return conv

def cnn1d(X, n_filters, dropOut):
    with tf.variable_scope("cnn1d"):
        conv1 = conv1d(X, n_filters, 5, name="conv1")
        print (conv1.get_shape())
        # conv1 = tf.nn.dropout(conv1, keep_prob=1-dropOut)

        conv2 = conv1d(conv1 ,n_filters, 3, name="conv2", strides=True)
        print (conv2.get_shape())
        # conv2 = tf.nn.dropout(conv2, keep_prob=1-dropOut)

        conv3 = conv1d(conv2 ,n_filters*2, 3, name="conv3")
        print (conv3.get_shape())
        # conv3 = tf.nn.dropout(conv3, keep_prob=1-dropOut)

        conv4 = conv1d(conv3 ,n_filters*2, 3, name="conv4",strides=True)
        print (conv4.get_shape())
        # conv4 = tf.nn.dropout(conv4, keep_prob=1-dropOut)

        conv5 = conv1d(conv4 ,n_filters*4, 3, name="conv5")
        print (conv5.get_shape())

        conv6 = conv1d(conv5 ,n_filters*4, 1, name="conv6",strides=True)
        print (conv6.get_shape())

        flatten = tf.keras.layers.AveragePooling1D(pool_size=2, strides=None, padding='valid', data_format='channels_last')(conv6)
        flatten = tf.nn.dropout(flatten, keep_prob=1-dropOut)
        print (flatten.get_shape())

        pred = tf.keras.layers.Dense(1)(flatten)

    return pred

# def rnn_model (X, num_units, dropOut) :
#     X_seq = tf.unstack(X,axis=1)
#     cell = tf.nn.rnn_cell.LSTMCell(num_units,activation=tf.nn.relu)
#     cell = tf.nn.rnn_cell.DropoutWrapper(cell,output_keep_prob=1-dropOut, state_keep_prob=1-dropOut)
#     outputs, _ = tf.nn.static_rnn(cell, X_seq, dtype=tf.float32)
#     output = outputs[-1]
#     pred = tf.keras.layers.Dense(1)(output)
#     return pred

def run (train_radar_X,test_radar_X,train_opt_X,test_opt_X,train_indices_X,test_indices_X,train_y,test_y,
                                n_units,batch_size,n_epochs,lr,drop,model_directory,split_numb,fold_numb) :

    X_rad = tf.compat.v1.placeholder(tf.float32,shape=(None,37,2),name='X_rad')
    X_opt = tf.compat.v1.placeholder(tf.float32,shape=(None,37,10),name='X_opt')
    X_indices = tf.compat.v1.placeholder(tf.float32,shape=(None,37,7),name='X_indices')

    y = tf.compat.v1.placeholder(tf.float32,shape=(None),name='y')
    dropOut = tf.compat.v1.placeholder(tf.float32, shape=(), name="drop_rate")

    logits = cnn1d(X_opt,n_units,dropOut)
    # logits_rad = rnn_model(X_rad,n_units,dropOut)
    # logits = cnn1d(X_opt,n_units,dropOut)
    # logits_indices = cnn1d(X_indices,n_units,dropOut)

    with tf.variable_scope("pred"):

        pred = tf.identity(logits,name="prediction")
        # pred_opt = tf.identity(logits_opt,name="prediction")
        # pred = tf.identity(logits_indices,name="prediction")

        # pred = tf.reduce_mean(tf.concat([pred_rad,pred_opt,pred_indices],axis=1),axis=1)
        # print (pred.get_shape())

    with tf.variable_scope("cost"):
        cost = tf.reduce_mean(tf.math.squared_difference(y,pred))
        # cost = tf.reduce_mean(tf.losses.absolute_difference(y,pred)) 
        # cost += .3 * tf.reduce_mean(tf.losses.absolute_difference(y,pred_opt))
        # cost += .3 * tf.reduce_mean(tf.losses.absolute_difference(y,pred_indices)) 

    # optimizer = tf.train.AdamOptimizer(learning_rate=lr).minimize(cost)
    optimizer = tf.train.AdamOptimizer(learning_rate=lr).minimize(cost)

    ##############################################################################

    n_batch = int(train_radar_X.shape[0]/batch_size)
    if train_radar_X.shape[0] % batch_size != 0:
        n_batch+=1
    print ("n_batch: %d" %n_batch)

    saver = tf.train.Saver()
    best_r2 = sys.float_info.min
    best_rmse = sys.float_info.max

    init = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init)

        for epoch in range(1,n_epochs+1):
            start = time.time()
            epoch_loss = 0

            train_radar_X, train_opt_X, train_indices_X, train_y = shuffle (train_radar_X,
                                    train_opt_X, train_indices_X, train_y, random_state=0)
            train_pred = None
            for batch in range(n_batch):
                batch_X_rad = get_batch(train_radar_X,batch,batch_size)
                batch_X_opt = get_batch(train_opt_X,batch,batch_size)
                batch_X_indices = get_batch(train_indices_X,batch,batch_size)
                batch_y = get_batch(train_y,batch,batch_size)

                loss, batch_pred,_ = session.run([cost,pred,optimizer], feed_dict={X_rad:batch_X_rad,
                                                                   X_opt:batch_X_opt,
                                                                   X_indices:batch_X_indices,
                                                                   y:batch_y,
                                                                   dropOut:drop
                                                                  })

                del batch_X_rad, batch_X_opt, batch_X_indices, batch_y

                if train_pred is None :
                    train_pred = batch_pred.reshape(batch_pred.shape[0])
                else :
                    train_pred = np.hstack((train_pred,batch_pred.reshape(batch_pred.shape[0])))
                # print (total_pred.shape)
                epoch_loss += loss

            stop = time.time()
            elapsed = stop - start
            train_r2 = r2_score(train_y,train_pred)
            print ("Epoch ",epoch, " Train loss:",epoch_loss/n_batch, " Train R2: ",train_r2, " Time: ",elapsed)

            #############################################################################
            # r2 = r2_score(train_y,total_pred)
            # if r2>0:
            #     print ("TRAIN R2: %f" %r2_score(train_y,total_pred))
            # if r2 >= 0.5:
            #     print (train_y[:10])
            #     print (total_pred[:10])

            test_batch = int(test_radar_X.shape[0] / (batch_size))
            if test_radar_X.shape[0] % (batch_size) != 0:
                test_batch+=1

            total_pred = None
            for ibatch in range(test_batch):
                test_batch_X_rad = get_batch(test_radar_X,ibatch,batch_size)
                test_batch_X_opt = get_batch(test_opt_X,ibatch,batch_size)
                test_batch_X_indices = get_batch(test_indices_X,ibatch,batch_size)

                test_batch_pred = session.run(pred,feed_dict={X_rad:test_batch_X_rad,
                                                         X_opt:test_batch_X_opt,
                                                         X_indices:test_batch_X_indices,
                                                         dropOut:0.})

                del test_batch_X_rad, test_batch_X_opt, test_batch_X_indices

                if total_pred is None :
                    total_pred = test_batch_pred.reshape(test_batch_pred.shape[0])
                else :
                    total_pred = np.hstack((total_pred,test_batch_pred.reshape(test_batch_pred.shape[0])))

            test_r2 = r2_score(test_y,total_pred)
            test_rmse = np.sqrt(mean_squared_error(test_y*1000,total_pred*1000))
            if test_r2 > 0:
                print ("PREDICTION")
                # print (test_y[:10])
                # print (total_pred[:10])
                print ("TEST R2: %f" %test_r2)
                print ("TEST RMSE:%f"%test_rmse)
                # print ("TEST MSE: %f"%mean_squared_error(test_y,total_pred))
                # print ("TEST MAE: %f"%mean_absolute_error(test_y,total_pred))
                if test_r2 > best_r2 and test_rmse < best_rmse :
                    save_path = saver.save(session, model_directory+"/model_"+str(split_numb)+"_fold-"+str(fold_numb))
                    print("Model saved in path: %s" % save_path)
                    best_r2 = test_r2
                    best_rmse = test_rmse

def restore (test_X_rad, test_X_opt, test_X_indices, test_y, model_directory, split_numb, fold_numb, batch_size) :
    
    ckpt_path = os.path.join(model_directory,"model_%s_fold-%s"%(str(split_numb),str(fold_numb)))
    
    results_path = os.path.join(model_directory,"results")
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    tf.reset_default_graph()
    with tf.Session() as session :

        model_saver = tf.train.import_meta_graph(ckpt_path+".meta")
        model_saver.restore(session, ckpt_path)
        
        graph = tf.get_default_graph()

        X_rad = graph.get_tensor_by_name("X_rad:0")
        X_opt = graph.get_tensor_by_name("X_opt:0")
        X_indices = graph.get_tensor_by_name("X_indices:0")
        dropOut = graph.get_tensor_by_name("drop_rate:0")
        prediction = graph.get_tensor_by_name("pred/prediction:0")
        print ("Model restored. "+ckpt_path)

        n_batch = int(test_X_rad.shape[0] / batch_size)
        if test_X_rad.shape[0] % batch_size != 0:
            n_batch+=1
        print ("n_batch: %d" %n_batch)

        total_pred = None
        for batch in range(n_batch):
            batch_X_rad = get_batch(test_X_rad,batch,batch_size)
            batch_X_opt = get_batch(test_X_opt,batch,batch_size)
            batch_X_indices = get_batch(test_X_indices,batch,batch_size)

            batch_pred = session.run(prediction,feed_dict={X_rad:batch_X_rad,
                                                            X_opt:batch_X_opt,
                                                            X_indices:batch_X_indices,
                                                            dropOut:0.})
            del batch_X_rad, batch_X_opt, batch_X_indices

            if total_pred is None :
                total_pred = batch_pred.reshape(batch_pred.shape[0])
            else : 
                total_pred = np.hstack((total_pred,batch_pred.reshape(batch_pred.shape[0])))

    total_pred = total_pred * 1000
    r2 = r2_score(test_y,total_pred)
    rmse = np.sqrt(mean_squared_error(test_y,total_pred))
    print ("R2: %f" %r2)
    print ("RMSE:%f"%rmse)
    np.save(os.path.join(results_path,"pred_"+str(split_numb)+"_fold-%s"%fold_numb+".npy"),total_pred)
    np.save(os.path.join(results_path,"obs_"+str(split_numb)+"_fold-%s"%fold_numb+".npy"),test_y)



if __name__ == '__main__' :

    # Reading data
    train_radar_X1 = np.load(sys.argv[1])
    train_opt_X1 = np.load(sys.argv[2])
    train_indices_X1 = np.load(sys.argv[3])
    train_y1 = np.load(sys.argv[4])

    test_radar_X1 = np.load(sys.argv[5])
    test_opt_X1 = np.load(sys.argv[6])
    test_indices_X1 = np.load(sys.argv[7])
    test_y1 = np.load(sys.argv[8])

    train_radar_X2 = np.load(sys.argv[9])
    train_opt_X2 = np.load(sys.argv[10])
    train_indices_X2 = np.load(sys.argv[11])
    train_y2 = np.load(sys.argv[12])

    test_radar_X2 = np.load(sys.argv[13])
    test_opt_X2 = np.load(sys.argv[14])
    test_indices_X2 = np.load(sys.argv[15])
    test_y2 = np.load(sys.argv[16])

    train_radar_X3 = np.load(sys.argv[17])
    train_opt_X3 = np.load(sys.argv[18])
    train_indices_X3 = np.load(sys.argv[19])
    train_y3 = np.load(sys.argv[20])

    test_radar_X3 = np.load(sys.argv[21])
    test_opt_X3 = np.load(sys.argv[22])
    test_indices_X3 = np.load(sys.argv[23])
    test_y3 = np.load(sys.argv[24])

    valid_radar_X = np.load(sys.argv[25])
    valid_opt_X = np.load(sys.argv[26])
    valid_indices_X = np.load(sys.argv[27])
    valid_y = np.load(sys.argv[28])

    model_directory = sys.argv[29]
    if not os.path.exists(model_directory):
        os.makedirs(model_directory)
    split_numb = sys.argv[30]
    fold_numb = sys.argv[31]

    sys.stdout.flush

    # Formatting
    train_radar_X = format_X_3D([train_radar_X1,train_radar_X2])
    train_opt_X = format_X_3D([train_opt_X1,train_opt_X2])
    train_indices_X = format_X_3D([train_indices_X1,train_indices_X2])

    train_y = format_y([train_y1,train_y2],target=-1)

    test_radar_X = format_X_3D([test_radar_X1,test_radar_X2])
    test_opt_X = format_X_3D([test_opt_X1,test_opt_X2])
    test_indices_X = format_X_3D([test_indices_X1,test_indices_X2])

    test_y = format_y([test_y1,test_y2],target=-1)

    # valid_radar_X = format_X_3D([valid_radar_X])
    # valid_opt_X = format_X_3D([valid_opt_X])
    # valid_indices_X = format_X_3D([valid_indices_X])

    # valid_y = valid_y[:,-1]

    # radar_X = np.vstack((train_radar_X,test_radar_X))
    # opt_X = np.vstack((train_opt_X,test_opt_X))
    # indices_X = np.vstack((train_indices_X,test_indices_X))
    # y = np.hstack((train_y,test_y))

    # Run Model
    n_units = 64
    batch_size = 1
    n_epochs = 1000
    lr = 1E-3
    drop = 0.5

    run (train_radar_X,test_radar_X,train_opt_X,test_opt_X,train_indices_X,test_indices_X,train_y,test_y,
                                                                    n_units,batch_size,n_epochs,lr,drop,model_directory,split_numb,fold_numb)

    restore (test_radar_X, test_opt_X, test_indices_X, test_y*1000, model_directory, split_numb, fold_numb, batch_size)
