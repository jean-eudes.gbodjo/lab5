import sys
import time
import os
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

def format_to_cnn (X):
    dim = int(np.sqrt(X.shape[1]))
    X = X.reshape(X.shape[0],dim,dim,X.shape[2],X.shape[3])
    print (X.shape)
    return X

def format_label (y,target=3,scale_by=1000):
    y = y[:,target]/scale_by
    print (y.shape)
    return y

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def cnn2d(X):
    W1 = tf.compat.v1.get_variable("W1",[5,5,2,32],dtype=tf.float32,initializer=tf.contrib.layers.xavier_initializer())
    W2 = tf.compat.v1.get_variable("W2",[3,3,32,64],dtype=tf.float32,initializer=tf.contrib.layers.xavier_initializer())

    b1 = tf.compat.v1.get_variable("b1",initializer=tf.compat.v1.random.normal([32]))
    b2 = tf.compat.v1.get_variable("b2",initializer=tf.compat.v1.random.normal([64]))
    
    conv1 = tf.nn.conv2d(X,W1,padding='VALID')
    conv1 = tf.nn.bias_add(conv1, b1)
    conv1 = tf.keras.layers.BatchNormalization(name="bn1")(conv1)
    conv1 = tf.nn.relu(conv1)
    print (conv1.get_shape())

    max_pool = tf.compat.v1.layers.max_pooling2d(conv1,2,strides=2,padding='VALID')
    print (max_pool.get_shape())

    conv2 = tf.nn.conv2d(max_pool,W2,padding='VALID')
    conv2 = tf.nn.bias_add(conv2, b2)
    conv2 = tf.keras.layers.BatchNormalization(name="bn2")(conv2)
    conv2 = tf.nn.relu(conv2)
    print (conv2.get_shape())

    flatten = tf.keras.layers.Flatten()(conv2)
    print (flatten.get_shape())

    return flatten

def add_fc(features,units,dropOut):
    fc1 = tf.keras.layers.Dense(units,activation=tf.nn.relu)(features)
    fc1 = tf.nn.dropout(fc1, keep_prob=1-dropOut)
    fc2 = tf.keras.layers.Dense(units/2,activation=tf.nn.relu)(fc1)
    fc2 = tf.nn.dropout(fc2, keep_prob=1-dropOut)
    pred = tf.keras.layers.Dense(1)(fc2)
    return pred


def run(train_radar_X,train_opt_X,train_indices_X,train_y,fc_units,batch_size,n_epochs,lr,drop):
    dim = train_radar_X.shape[1]
    X = tf.compat.v1.placeholder(tf.float32,shape=(None,dim,dim,None),name='X')
    y = tf.compat.v1.placeholder(tf.float32,shape=(None),name='y')
    dropOut = tf.compat.v1.placeholder(tf.float32, shape=(), name="drop_rate")

    feat = cnn2d(X)

    with tf.variable_scope("pred"):
        logits = add_fc(feat,fc_units,dropOut)
        pred = tf.identity(logits,name="prediction")

    with tf.variable_scope("cost"):
        cost = tf.reduce_mean(tf.math.squared_difference(y,pred))

    optimizer = tf.train.AdamOptimizer(learning_rate=lr).minimize(cost)

    ##############################################################################

    n_batch = int(train_radar_X.shape[0]/batch_size)
    if train_radar_X.shape[0] % batch_size != 0:
        n_batch+=1
    print ("n_batch: %d" %n_batch)

    # saver = tf.train.Saver()
    # best_r2 = sys.float_info.min

    init = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init)

        for epoch in range(1,n_epochs+1):
            start = time.time()
            epoch_loss = 0

            train_radar_X, train_opt_X, train_indices_X, train_y = shuffle (train_radar_X, 
                                    train_opt_X, train_indices_X, train_y, random_state=0)
            total_pred = None
            for batch in range(n_batch):
                batch_X_rad = get_batch(train_radar_X,batch,batch_size)
                batch_X_opt = get_batch(train_opt_X,batch,batch_size)
                batch_X_indices = get_batch(train_indices_X,batch,batch_size)
                batch_y = get_batch(train_y,batch,batch_size)

                loss, batch_pred,_ = session.run([cost,pred,optimizer], feed_dict={X:batch_X_rad,
                                                                   y:batch_y,
                                                                   dropOut:drop
                                                                  })

                del batch_X_rad, batch_X_opt, batch_X_indices, batch_y

                if total_pred is None :
                    total_pred = batch_pred.reshape(batch_pred.shape[0])
                else :
                    total_pred = np.hstack((total_pred,batch_pred.reshape(batch_pred.shape[0])))

                epoch_loss += loss

            stop = time.time()
            elapsed = stop - start
            print ("Epoch ",epoch, " Train loss:",epoch_loss/n_batch, "| Time: ",elapsed)

            #############################################################################

            # test_batch = int(test_radar_X.shape[0] / (batch_size))
            # if test_radar_X.shape[0] % (batch_size) != 0:
            #     test_batch+=1

            # total_pred = None
            # for ibatch in range(test_batch):
            #     test_batch_X_rad = get_batch(test_radar_X,ibatch,batch_size)
            #     test_batch_X_opt = get_batch(test_opt_X,ibatch,batch_size)
            #     test_batch_X_indices = get_batch(test_indices_X,ibatch,batch_size)

            #     batch_pred = session.run(pred,feed_dict={X_rad:test_batch_X_rad,
            #                                              X_opt:test_batch_X_opt,
            #                                              X_indices:test_batch_X_indices,
            #                                              dropOut:0.})

            #     del test_batch_X_rad, test_batch_X_opt, test_batch_X_indices

            #     if total_pred is None :
            #         total_pred = batch_pred.reshape(batch_pred.shape[0])
            #     else :
            #         total_pred = np.hstack((total_pred,batch_pred.reshape(batch_pred.shape[0])))

            # print ("PREDICTION")
            # print (train_y[:10])
            # print (total_pred[:10])
            r2 = r2_score(train_y,total_pred)
            if r2>0:
                print ("TRAIN R2: %f" %r2_score(train_y,total_pred))
            # print ("TEST MSE: %f"%mean_squared_error(test_y,total_pred))
            # print ("TEST MAE: %f"%mean_absolute_error(test_y,total_pred))
    
if __name__ == '__main__' :

    # Reading data
    train_radar_X = np.load(sys.argv[1])
    train_opt_X = np.load(sys.argv[2])
    train_indices_X = np.load(sys.argv[3])
    train_y = np.load(sys.argv[4])
    sys.stdout.flush

    # Format data
    train_radar_X = format_to_cnn(train_radar_X)
    train_radar_X = train_radar_X[:,:,:,:2,8]
    # train_radar_X = train_radar_X.reshape(train_radar_X.shape[0],train_radar_X.shape[1],train_radar_X.shape[2],1)
    print(train_radar_X.shape)

    train_opt_X = format_to_cnn(train_opt_X)
    train_opt_X = train_opt_X[:,:,:,:1,10]
    print(train_opt_X.shape)

    train_indices_X = format_to_cnn(train_indices_X)
    train_indices_X = train_indices_X[:,:,:,:,10]
    print(train_indices_X.shape)

    train_y = format_label(train_y)

    # Run Model
    fc_units = 64
    batch_size = 8
    n_epochs = 1000
    lr = 1E-4
    drop = 0.5

    run(train_radar_X,train_opt_X,train_indices_X,train_y,fc_units,batch_size,n_epochs,lr,drop)