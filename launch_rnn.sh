# path_1="splits/niakhar_2017"
# path_2="splits/niakhar_2018"
# path_3="splits/nioro_2018"

# for i in {1..10}
# do  
#     echo "split"$i
#     for f in {1..3}
#     do
#         echo "fold"$f
#         echo python rnn.py $path_1/train_opt_$i\_fold-$f.npy $path_1/test_opt_$i\_fold-$f.npy \
#                            $path_2/train_opt_$i\_fold-$f.npy $path_2/test_opt_$i\_fold-$f.npy \
#                            $path_3/train_opt_$i\_fold-$f.npy $path_3/test_opt_$i\_fold-$f.npy \
#                            $path_1/train_radar_$i\_fold-$f.npy $path_1/test_radar_$i\_fold-$f.npy \
#                            $path_2/train_radar_$i\_fold-$f.npy $path_2/test_radar_$i\_fold-$f.npy \
#                            $path_3/train_radar_$i\_fold-$f.npy $path_3/test_radar_$i\_fold-$f.npy \
#                            $path_1/train_indices_$i\_fold-$f.npy $path_1/test_indices_$i\_fold-$f.npy \
#                            $path_2/train_indices_$i\_fold-$f.npy $path_2/test_indices_$i\_fold-$f.npy \
#                            $path_3/train_indices_$i\_fold-$f.npy $path_3/test_indices_$i\_fold-$f.npy \
#                            $path_1/train_yields_$i\_fold-$f.npy $path_1/test_yields_$i\_fold-$f.npy \
#                            $path_2/train_yields_$i\_fold-$f.npy $path_2/test_yields_$i\_fold-$f.npy \
#                            $path_3/train_yields_$i\_fold-$f.npy $path_3/test_yields_$i\_fold-$f.npy \
#                            $i $f rnn
#     done
# done

# path="patchs_data/1Ha"
# echo python cnn.py $path/nioro_2018_radar_patchs.npy $path/nioro_2018_opt_patchs.npy $path/nioro_2018_indices_patchs.npy $path/nioro_2018_yields.npy

# path1="./data/niakhar_2017"
# # path2="./data/niakhar-simco_2018"
# path2="./data/niakhar_2018"
# path3="./data/nioro_2018"

# echo python lstm_cnn1d.py $path1\_rad_seq.npy $path1\_opt_seq.npy $path1\_indices_seq.npy $path1\_yields.npy $path2\_rad_seq.npy $path2\_opt_seq.npy $path2\_indices_seq.npy $path2\_yields.npy $path3\_rad_seq.npy $path3\_opt_seq.npy $path3\_indices_seq.npy $path3\_yields.npy
# echo python autoencoders.py $path1\_rad_seq.npy $path1\_opt_seq.npy $path1\_indices_seq.npy $path1\_yields.npy $path2\_rad_seq.npy $path2\_opt_seq.npy $path2\_indices_seq.npy $path2\_yields.npy $path3\_rad_seq.npy $path3\_opt_seq.npy $path3\_indices_seq.npy $path3\_yields.npy

path1="./splits/niakhar_2017"
path2="./splits/niakhar-simco_2018"
path3="./splits/nioro_2018"
modelpath="./cnn1d"

path4="./data/niakhar-serena_2018"


for i in {1..10}
do
    for f in {1..3}
    do 
    python cnn1d.py \
    $path1/train_radar_$i\_fold-$f.npy $path1/train_opt_$i\_fold-$f.npy $path1/train_indices_$i\_fold-$f.npy $path1/train_yields_$i\_fold-$f.npy \
    $path1/test_radar_$i\_fold-$f.npy $path1/test_opt_$i\_fold-$f.npy $path1/test_indices_$i\_fold-$f.npy $path1/test_yields_$i\_fold-$f.npy \
    $path2/train_radar_$i\_fold-$f.npy $path2/train_opt_$i\_fold-$f.npy $path2/train_indices_$i\_fold-$f.npy $path2/train_yields_$i\_fold-$f.npy \
    $path2/test_radar_$i\_fold-$f.npy $path2/test_opt_$i\_fold-$f.npy $path2/test_indices_$i\_fold-$f.npy $path2/test_yields_$i\_fold-$f.npy \
    $path3/train_radar_$i\_fold-$f.npy $path3/train_opt_$i\_fold-$f.npy $path3/train_indices_$i\_fold-$f.npy $path3/train_yields_$i\_fold-$f.npy \
    $path3/test_radar_$i\_fold-$f.npy $path3/test_opt_$i\_fold-$f.npy $path3/test_indices_$i\_fold-$f.npy $path3/test_yields_$i\_fold-$f.npy \
    $path4\_rad_seq.npy $path4\_opt_seq.npy $path4\_indices_seq.npy $path4\_yields.npy \
    $modelpath $i $f
    done
done

