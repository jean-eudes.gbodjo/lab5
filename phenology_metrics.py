import os
import numpy as np
import pandas as pd
import scipy as sp
from scipy import sparse, linalg
import matplotlib.pyplot as plt

def extract_metrics (csv,lstBands,start_date,end_date,sos_treshold,eos_treshold):
    ptrn = os.path.basename(csv)
    outFolder = "./metrics"
    if not os.path.exists(outFolder):
        os.makedirs(outFolder)
    
    times = pd.date_range(start=start_date, end = end_date, freq='5D')
    
    df = pd.read_csv(csv)

    outdf = None
    outDict = {}
    
    for band in lstBands:
        for index,row in df.iterrows():
            lstValues=[]
            for i in range(len(times)):
                date = band+'_'+str(times.strftime('%Y%m%d')[i])
                lstValues.append(row[date])

            plot_df = pd.DataFrame({"Date":times,"Value":lstValues})

            # Phenology Metrics 
            band_min = plot_df.Value.min()
            band_max = plot_df.Value.max()

            # SOS 
            for k in range(len(times)):
                ratio_sos = (plot_df.Value[k] - band_min)/(band_max-band_min)
                if (ratio_sos >= sos_treshold):
                    break
            SOS = int(plot_df["Date"][k].strftime('%j'))
            SOS_index = k

            # EOS 
            for k in range(len(times)):
                ratio_eos = (plot_df.Value[len(times)-(k+1)] - band_min)/(band_max-band_min)
                if (ratio_eos >= eos_treshold):
                    break
            EOS = int(plot_df["Date"][len(times)-(k+1)].strftime('%j'))
            EOS_index = len(times)-(k+1)

            # LOS
            LOS = EOS - SOS

            # AMPL
            AMPL = band_max - band_min

            # Date MAX 
            MAX_index = plot_df.loc[plot_df["Value"]==band_max].index[0]
            MAX = int(plot_df["Date"][MAX_index].strftime('%j'))

            # Integrated values
            CUM_SOS_MAX = 0
            for m in range(SOS_index,MAX_index+1) :
                CUM_SOS_MAX += plot_df["Value"][m]

            CUM_MAX_EOS = 0
            for m in range(MAX_index,EOS_index+1) :
                CUM_MAX_EOS += plot_df["Value"][m]
            
            CUM_SOS_EOS = 0
            for m in range(SOS_index,EOS_index+1) :
                CUM_SOS_EOS += plot_df["Value"][m]
            
            # RATE_SOS_MAX Value
            RATE_SOS_MAX = 100 * (band_max - plot_df["Value"][SOS_index]) / plot_df["Value"][SOS_index]
            
            # RATE_MAX_EOS Value
            RATE_MAX_EOS = 100 * (plot_df["Value"][EOS_index] - band_max) / band_max

            outDict.setdefault('feat_index',[]).append(index)
            
            if band == lstBands[0] :
                outDict.setdefault('ID',[]).append(row['ID'])
                outDict.setdefault('Projet',[]).append(row['Projet'])
                outDict.setdefault('Biom_f',[]).append(row['Biom_f'])
                outDict.setdefault('Biom_s',[]).append(row['Biom_s'])
                outDict.setdefault('Rdt_f',[]).append(row['Rdt_f'])
                outDict.setdefault('Rdt_s',[]).append(row['Rdt_s'])

            outDict.setdefault('%s_SOS'%band,[]).append(SOS)
            outDict.setdefault('%s_EOS'%band,[]).append(EOS)
            outDict.setdefault('%s_LOS'%band,[]).append(LOS)
            outDict.setdefault('%s_MAX'%band,[]).append(MAX)
            outDict.setdefault('%s_MAX_VALUE'%band,[]).append(band_max)
            outDict.setdefault('%s_AMPL'%band,[]).append(AMPL)
            outDict.setdefault('%s_CUM_SOS_MAX'%band,[]).append(CUM_SOS_MAX)
            outDict.setdefault('%s_CUM_MAX_EOS'%band,[]).append(CUM_MAX_EOS)
            outDict.setdefault('%s_CUM_SOS_EOS'%band,[]).append(CUM_SOS_EOS)
            outDict.setdefault('%s_RATE_SOS_MAX'%band,[]).append(RATE_SOS_MAX)
            outDict.setdefault('%s_RATE_MAX_EOS'%band,[]).append(RATE_MAX_EOS)

            # Short Integrated values 
            # From SOS to 100 days after SOS, with SOS + 5 days shift
            # for m in range (20): # 0 to 100/5 
            #     for l in range (m+1,m+21):
            #         if 5*l <= 100 :
            #             CUM = 0
            #             for n in range (5*m,5*l):
            #                 CUM += plot_df["Value"][SOS_index+n]
            #             # print ('CUM_%s_%s'%(str(5*m),str(5*l)))
            #             outDict.setdefault('%s_CUM_%s_%s'%(band,str(5*m),str(5*l)),[]).append(CUM)
        if outdf is None :
            outdf = pd.DataFrame.from_dict(outDict)
        else:
            outdf = outdf.merge(pd.DataFrame.from_dict(outDict),on='feat_index',how='inner')

    outCSV = os.path.join(outFolder,'{}'.format(ptrn.replace(".csv","_phenology_metrics.csv")))
    outdf = outdf.drop(columns='feat_index')
    outdf.to_csv(outCSV,index=False)
            
if __name__=='__main__':
    
    csv = "./interpolate/niakhar_2017_opt_gapf_notree_interpolate.csv"
    sos_treshold = 0.1
    eos_treshold = 0.8
    extract_metrics(csv,["NDVI"],"20170502","20171030",sos_treshold,eos_treshold)


