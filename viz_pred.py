import matplotlib.pyplot as plt 
import numpy as np 
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

yields_obs = np.load('./cnn1d/results/opt/obs_1_fold-3.npy')
# yields_obs = np.vstack((yields_obs,np.load('./splits/niakhar-simco_2018/test_yields_1_fold-1.npy')))
# yields_obs = yields_obs[:,-1]
print(yields_obs.shape)

yields_pred = np.load('./cnn1d/results/opt/pred_1_fold-3.npy')
print (yields_pred.shape)

print ("R2:",r2_score(yields_obs,yields_pred))
print ("MAE:",mean_absolute_error(yields_obs,yields_pred))
# print ("MSE:",mean_squared_error(yields_obs,yields_pred))
print ("RMSE:",np.sqrt(mean_squared_error(yields_obs,yields_pred)))

plt.figure()
plt.plot(yields_pred,yields_obs,'o')
x=np.arange(100,3000)
y=np.arange(100,3000)
plt.plot(x,y)
plt.xlabel('pred')
plt.ylabel('obs')
plt.show()