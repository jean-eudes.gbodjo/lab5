import numpy as np 
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

path = './cnn1d/results/opt/%s_%s_fold-%s.npy'

r2 = []
mae = []
rmse = []
for i in range(10):
    tmp_r2 = []
    tmp_mae = []
    tmp_rmse = []
    for f in range(3):
        yields_obs = np.load(path%('obs',i+1,f+1))
        yields_pred = np.load(path%('pred',i+1,f+1))

        tmp_r2.append(r2_score(yields_obs,yields_pred))
        tmp_mae.append(mean_absolute_error(yields_obs,yields_pred))
        tmp_rmse.append(np.sqrt(mean_squared_error(yields_obs,yields_pred)))
    
    r2.append(tmp_r2)
    mae.append(tmp_mae)
    rmse.append(tmp_rmse)

r2 = np.array(r2)
mae = np.array(mae)
rmse = np.array(rmse)
print ("R2:",np.mean(np.mean(r2,axis=1)),np.std(np.std(r2,axis=1)))
print ("MAE:",np.mean(np.mean(mae,axis=1)),np.std(np.std(mae,axis=1)))
print ("RMSE:",np.mean(np.mean(rmse,axis=1)),np.std(np.std(rmse,axis=1)))

