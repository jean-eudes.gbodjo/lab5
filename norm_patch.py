import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

def norm_patch (radar_file, opt_file, plots_file, area, norm="meanstd", radar_bands=["VH","VV"],
                                    opt_bands=["B2","B3","B4","B8","B5","B6","B7","B8A","B11","B12"], 
                                    opt_indices = ["NDVI","NDWI","EVI","MSAVI2","GDVI","CIGreen","CIRedEdge"]):
    size = int(int(int(np.sqrt(area * 10000))/10)**2)
    print ("Size:",size)

    # Radar
    radar_array = np.load(radar_file)
    n_bands = len(radar_bands)
    n_timestamps = int(radar_array.shape[1]/(size*n_bands))

    norm_radar_array = None
    for b in range(n_bands) :
        ts_values = radar_array[:,b*size*n_timestamps:b*size*n_timestamps+size*n_timestamps]

        if norm == "meanstd":
            ts_values = (ts_values - ts_values.mean()) / ts_values.std()
        elif norm == "minmax":
            ts_values = (ts_values - ts_values.min()) / (ts_values.max() - ts_values.min())

        if norm_radar_array is None :
            norm_radar_array = ts_values
        else :
            norm_radar_array = np.hstack((norm_radar_array,ts_values))

    radar_patchs = []
    for i in range(n_timestamps):
        lst = []
        for j in range(n_bands):
            lst.append(norm_radar_array[:,i*size+j*n_timestamps*size:i*size+j*n_timestamps*size+size])
        radar_patchs.append(np.stack(lst, axis=-1))
    radar_patchs = np.stack(radar_patchs,axis=-1)
    print (radar_patchs.shape)

    # Opt Bands
    opt_array = np.load(opt_file)
    n_bands = len(opt_bands)
    n_timestamps = int(opt_array.shape[1]/(size*(len(opt_bands) + len(opt_indices))))

    norm_opt_array = None
    for b in range(len(opt_bands)) :
        ts_values = opt_array[:,b*size*n_timestamps:b*size*n_timestamps+size*n_timestamps]

        if norm == "meanstd":
            ts_values = (ts_values - ts_values.mean()) / ts_values.std()
        elif norm == "minmax":
            ts_values = (ts_values - ts_values.min()) / (ts_values.max() - ts_values.min())

        if norm_opt_array is None :
            norm_opt_array = ts_values
        else :
            norm_opt_array = np.hstack((norm_opt_array,ts_values))

    opt_patchs = []
    for i in range(n_timestamps):
        lst = []
        for j in range(n_bands):
            lst.append(norm_opt_array[:,i*size+j*n_timestamps*size:i*size+j*n_timestamps*size+size])
        opt_patchs.append(np.stack(lst, axis=-1))
    opt_patchs = np.stack(opt_patchs,axis=-1)
    print (opt_patchs.shape)

    # Opt Indices
    indices_array = np.load(opt_file)
    n_bands = len(opt_indices)
    n_timestamps = int(opt_array.shape[1]/(size*(len(opt_bands) + len(opt_indices))))

    norm_indices_array = None
    for b in range(len(opt_bands),len(opt_bands)+len(opt_indices)) :
        ts_values = indices_array[:,b*size*n_timestamps:b*size*n_timestamps+size*n_timestamps]

        if norm == "meanstd":
            ts_values = (ts_values - ts_values.mean()) / ts_values.std()
        elif norm == "minmax":
            ts_values = (ts_values - ts_values.min()) / (ts_values.max() - ts_values.min())

        if norm_indices_array is None :
            norm_indices_array = ts_values
        else :
            norm_indices_array = np.hstack((norm_indices_array,ts_values))

    indices_patchs = []
    for i in range(n_timestamps):
        lst = []
        for j in range(n_bands):
            lst.append(norm_indices_array[:,i*size+j*n_timestamps*size:i*size+j*n_timestamps*size+size])
        indices_patchs.append(np.stack(lst, axis=-1))
    indices_patchs = np.stack(indices_patchs,axis=-1)
    print (indices_patchs.shape)

    outPath = "./patchs_data/{}Ha".format(area)
    if not os.path.exists(outPath):
        os.makedirs(outPath)

    ptrn = os.path.basename(radar_file).split('_')[0]+"_"+os.path.basename(radar_file).split('_')[1]
    
    np.save(os.path.join(outPath,"{}_radar_patchs.npy".format(ptrn)),radar_patchs)
    np.save(os.path.join(outPath,"{}_opt_patchs.npy".format(ptrn)),opt_patchs)
    np.save(os.path.join(outPath,"{}_indices_patchs.npy".format(ptrn)),indices_patchs)
    
    plots_df = pd.read_csv(plots_file)
    rdt = plots_df[["Biom_f","Biom_s","Rdt_f","Rdt_s"]].values
    print (rdt.shape)
    np.save(os.path.join(outPath,"{}_yields.npy".format(ptrn)),rdt)

if __name__ == '__main__' :
    
    area=1
    # Niakhar 2017
    radar_file = "./patchs/%sHa/niakhar_2017_radar.npy"%area
    opt_file = "./patchs/%sHa/niakhar_2017_opt_gapf.npy"%area
    plots_file = "./patchs/%sHa/niakhar_2017_plots.csv"%area
    norm_patch(radar_file,opt_file,plots_file,area,norm="minmax")

    # Niakhar 2018

    radar_file = "./patchs/%sHa/niakhar_2018_radar.npy"%area
    opt_file = "./patchs/%sHa/niakhar_2018_opt_gapf.npy"%area
    plots_file = "./patchs/%sHa/niakhar_2018_plots.csv"%area
    norm_patch(radar_file,opt_file,plots_file,area,norm="minmax")

    radar_file = "./patchs/%sHa/niakhar-serena_2018_radar.npy"%area
    opt_file = "./patchs/%sHa/niakhar-serena_2018_opt_gapf.npy"%area
    plots_file = "./patchs/%sHa/niakhar-serena_2018_plots.csv"%area
    norm_patch(radar_file,opt_file,plots_file,area,norm="minmax")

    radar_file = "./patchs/%sHa/niakhar-simco_2018_radar.npy"%area
    opt_file = "./patchs/%sHa/niakhar-simco_2018_opt_gapf.npy"%area
    plots_file = "./patchs/%sHa/niakhar-simco_2018_plots.csv"%area
    norm_patch(radar_file,opt_file,plots_file,area,norm="minmax")

    # Nioro 2018

    radar_file = "./patchs/%sHa/nioro_2018_radar.npy"%area
    opt_file = "./patchs/%sHa/nioro_2018_opt_gapf.npy"%area
    plots_file = "./patchs/%sHa/nioro_2018_plots.csv"%area
    norm_patch(radar_file,opt_file,plots_file,area,norm="minmax")